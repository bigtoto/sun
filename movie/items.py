# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class MovieItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    m_name = scrapy.Field()
    m_url = scrapy.Field()
    m_time = scrapy.Field()
    m_yiming = scrapy.Field()
    m_pianming = scrapy.Field()
    m_chandi = scrapy.Field()
    m_leibie = scrapy.Field()
    m_yuyan = scrapy.Field()
    m_zimu = scrapy.Field()
    m_syrq = scrapy.Field()
    imdb_src = scrapy.Field()
    douban_src = scrapy.Field()
    wjgs = scrapy.Field()
    plet = scrapy.Field()
    m_size = scrapy.Field()
    m_pc = scrapy.Field()
    m_dy = scrapy.Field()
    m_actor = scrapy.Field()


    
