# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
from pymongo import MongoClient

class MoviePipeline(object):
	client = pymongo.MongoClient('mongodb://localhost:27017/')
	db = client["test"]
	collect = db["sites"]
	def process_item(self, item, spider):
		self.collect.insert_one(dict(item))
